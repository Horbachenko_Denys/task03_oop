package com.epam.controller;

import com.epam.model.House;

import java.util.List;

public interface Controller {
    List<House> getHouseList();

    List<House> sortHouseListByPrice(int amount);

    List<House> sortHouseListByDistanceToKindergarten(int amount);

    List<House> sortHouseListByDistanceToSchool(int amount);

    List<House> sortHouseListByDistanceToPlayground(int amount);
}
