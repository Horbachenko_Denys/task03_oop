package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.House;
import com.epam.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List<House> getHouseList() {
        return model.getHouseList();
    }

    @Override
    public List<House> sortHouseListByPrice(int amount) {
        return model.sortHouseListByPrice(amount);
    }

    @Override
    public List<House> sortHouseListByDistanceToKindergarten(int amount) {
        return model.sortHouseListByDistanceToKindergarten(amount);
    }

    @Override
    public List<House> sortHouseListByDistanceToSchool(int amount) {
        return model.sortHouseListByDistanceToSchool(amount);
    }

    @Override
    public List<House> sortHouseListByDistanceToPlayground(int amount) {
        return model.sortHouseListByDistanceToPlayground(amount);
    }
}
