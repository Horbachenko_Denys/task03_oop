package com.epam.model.comparator;

import com.epam.model.House;

import java.io.Serializable;
import java.util.Comparator;

public class DistanceToPlaygroundComparator implements Comparator<House>, Serializable {
    @Override
    public int compare(House h1, House h2) {
        return Integer.compare(h1.getDisToPlayground(), h2.getDisToPlayground());
    }
}
