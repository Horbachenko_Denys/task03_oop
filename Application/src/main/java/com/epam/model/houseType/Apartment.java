package com.epam.model.houseType;

import com.epam.model.House;

import java.util.Random;

public class Apartment extends House {
    public Apartment() {
        Random rnd = new Random();
        this.setPriceOfRent((rnd.nextInt(3) + 2) * 100);
        this.setDisToKindergarten((rnd.nextInt(10) + 2) * 100);
        this.setDisToSchool((rnd.nextInt(11) + 5) * 100);
        this.setDisToPlayground((rnd.nextInt(4) + 1) * 100);
    }

    @Override
    public String toString() {
        return String.format("%n" + "%s:price is:%d$, distance to garden:%dm, distance to school:%dm, distance to playground:%dm", this.getClass().getSimpleName(), getPriceOfRent(), getDisToKindergarten(), getDisToSchool(), getDisToPlayground());
    }
}
